
pub fn draw_playground(playground: & Vec<Vec<char>>){
    let table = table!(
    [" ", "a", "b", "c", "d", "e", "f", "g", "h"],
    [0, playground[0][0], playground[0][1], playground[0][2], playground[0][3], playground[0][4], playground[0][5], playground[0][6], playground[0][7]],
    [1, playground[1][0], playground[1][1], playground[1][2], playground[1][3], playground[1][4], playground[1][5], playground[1][6], playground[1][7]],
    [2, playground[2][0], playground[2][1], playground[2][2], playground[2][3], playground[2][4], playground[2][5], playground[2][6], playground[2][7]],
    [3, playground[3][0], playground[3][1], playground[3][2], playground[3][3], playground[3][4], playground[3][5], playground[3][6], playground[3][7]],
    [4, playground[4][0], playground[4][1], playground[4][2], playground[4][3], playground[4][4], playground[4][5], playground[4][6], playground[4][7]],
    [5, playground[5][0], playground[5][1], playground[5][2], playground[5][3], playground[5][4], playground[5][5], playground[5][6], playground[5][7]],
    [6, playground[6][0], playground[6][1], playground[6][2], playground[6][3], playground[6][4], playground[6][5], playground[6][6], playground[6][7]],
    [7, playground[7][0], playground[7][1], playground[7][2], playground[7][3], playground[7][4], playground[7][5], playground[7][6], playground[7][7]]
    );

    table.printstd();
}