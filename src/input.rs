use std::io;

pub fn movement() -> (usize, usize){
    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input)
            .expect("Failed to read line");
        if input.len() == 3 {
            let mut char_vec: Vec<char> = input.chars().collect();
            let mut first_digit = char_vec[0];
            match first_digit {
                'a'=> first_digit = '0',
                'b'=> first_digit = '1',
                'c'=> first_digit = '2',
                'd'=> first_digit = '3',
                'e'=> first_digit = '4',
                'f'=> first_digit = '5',
                'g'=> first_digit = '6',
                'h'=> first_digit = '7',
                _ => first_digit = 'f',
            }
            let mut last_digit = char_vec[1];
            match last_digit {
                '0'|'1'|'2'|'3'|'4'|'5'|'6'|'7' => {},
                _ => last_digit = 'f',
            }

            if first_digit != 'f' && last_digit != 'f'{
                return (last_digit.to_digit(10).unwrap() as usize, first_digit.to_digit(10).unwrap() as usize);
            } else{
                println!("Die Eingabe muss dem Format a1 entsprechen");
            }
        } else {
            println!("Die Eingabe muss dem Format a1 entsprechen");
        }
    }
}

pub fn decision() -> bool{
    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input)
            .expect("Failed to read line");
        if input.len() == 2 {
            let mut char_vec: Vec<char> = input.chars().collect();
            match char_vec[0]{
                'y'=> return true,
                'n'=> return false,
                _ => {println!("Die Eingabe muss y oder n lauten")}
            }
        } else {
            println!("Die Eingabe muss y oder n lauten");
        }
    }
}