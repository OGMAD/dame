#[macro_use]
extern crate prettytable;

mod fill_playground;
mod draw;
mod input;
mod input_validation;
mod detector;

fn main() {
    let mut playground = vec![vec![' '; 8]; 8];
    fill_playground::fill_playground(&mut playground);
    draw::draw_playground(&playground);

    let mut player = 'W';
    let mut enemy: (usize, usize) = (0, 0);
    let mut jump = false;

    loop {
        loop {
            println!("Wähle einen Stein!");
            let stone = input::movement();
            println!("Wo soll er hin?");
            let target = input::movement();
            if input_validation::movement(&playground, stone, target, player) {
                playground[target.0][target.1] = playground[stone.0][stone.1];
                playground[stone.0][stone.1] = ' ';
                break;
            } else if input_validation::jump(&playground, stone, target, player) {
                playground[target.0][target.1] = playground[stone.0][stone.1];
                playground[stone.0][stone.1] = ' ';
                enemy.0 = (stone.0 + target.0) / 2;
                enemy.1 = (stone.1 + target.1) / 2;
                playground[enemy.0][enemy.1] = ' ';
                jump = true;
                break;
            } else {
                println!("Dieser Zug war nicht möglich! Wähle einen anderen!");
            }
            draw::draw_playground(&playground);
        }
        playground = detector::dame(&mut playground);
        draw::draw_playground(&playground);
        if jump {
            println!("Kannst du erneut springen?");
            jump = input::decision();
        }
        if !jump {
            player = if player == 'W' { 'S' } else { 'W' };
        }
    }
}
