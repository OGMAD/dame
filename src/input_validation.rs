
pub fn movement(playground: & Vec<Vec<char>>, stone: (usize, usize), target: (usize, usize), player: char) -> bool {
    if playground[stone.0][stone.1] == player && playground[target.0][target.1] == ' ' {
        let mut x_diff = stone.0 as i8 - target.0 as i8;
        let mut y_diff = stone.1 as i8 - target.1 as i8;
        x_diff = if x_diff < 0 {x_diff * -1} else {x_diff};
        y_diff = if y_diff < 0 {y_diff * -1} else {y_diff};
        if x_diff == 1 && y_diff == 1 {true} else {false}
    } else if (playground[stone.0][stone.1] == 'Ψ' && player == 'W' || playground[stone.0][stone.1] == 'ζ' && player == 'S') && playground[target.0][target.1] == ' '{
        let mut x_diff = stone.0 as i8 - target.0 as i8;
        let mut y_diff = stone.1 as i8 - target.1 as i8;
        let counter = x_diff as usize;
        x_diff = if x_diff < 0 {x_diff * -1} else {x_diff};
        y_diff = if y_diff < 0 {y_diff * -1} else {y_diff};
        if x_diff == y_diff{
            for x in 1..counter{
                println!("{}", x);
                if playground[stone.0 + x][stone.1 + x] != ' '{return false}
            }
            true
        } else {false}
    } else {
        false
    }
}

pub fn jump(playground: & Vec<Vec<char>>, stone: (usize, usize), target: (usize, usize), player: char) -> bool {
    if playground[stone.0][stone.1] == player && playground[target.0][target.1] == ' ' {
        let mut x_diff = stone.0 as i8 - target.0 as i8;
        let mut y_diff = stone.1 as i8 - target.1 as i8;
        x_diff = if x_diff < 0 {x_diff * -1} else {x_diff};
        y_diff = if y_diff < 0 {y_diff * -1} else {y_diff};
        if x_diff == 2 && y_diff == 2 {true} else {false}
    } else {
        false
    }
}