pub fn fill_playground(mut playground: &mut Vec<Vec<char>>){
    for x in 0..8{
        for y in 0..8{
            if y % 2 != 0 && y != 3  && y != 4 {
                if x % 2 == 0 {
                    color_placement(x, y, &mut playground);
                }
            } else if y != 3  && y != 4{
                if x % 2 != 0 {
                    color_placement(x, y, &mut playground);
                }
            }

        }
    }
}

fn color_placement(x: usize, y: usize, playground: &mut Vec<Vec<char>>) {
    if y < 3{
        playground[y][x] = 'S'
    } else {
        playground[y][x] = 'Ψ'
    }
}